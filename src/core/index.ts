import {
  getAdsByHost,
  getCategoriesByHost,
  getConfigByHost,
  getDetailPostBySlug,
  getDetailSlug,
  getPostByCategories,
  getPostByCategoryByHost,
  getPostByHost,
  getRecentPostsByHost,
  getRecommendedPostsByHost,
  getReferenceByHost,
} from "./db";

const pageSize = 10;

export const getPosts = async (hostname = "", pathname = "", page = "1") => {
  const start = (parseInt(page) - 1) * pageSize;
  return getPostByHost(hostname, pathname, pageSize, start);
};

export const getDetail = async () => {
  return getDetailSlug();
};
export const getAllPostOfCategory = async (pathname: string) => {
  return await getPostByCategories(pathname);
};

export const getPostsByCategory = async (
  hostname = "",
  pathname = "",
  page = "1"
) => {
  const start = (parseInt(page) - 1) * pageSize;
  return getPostByCategoryByHost(hostname, pathname, pageSize, start);
};

export const getCategories = async (hostname = "") => {
  return getCategoriesByHost(hostname);
};

export const getRecentPosts = async (hostname = "") => {
  return getRecentPostsByHost(hostname);
};

export const getRecommendedPosts = async (hostname = "") => {
  return getRecommendedPostsByHost(hostname);
};

export const getAds = async (hostname = "") => {
  const ads = await getAdsByHost(hostname);
  const unit = JSON.parse(ads.unit);
  return { ...ads, unit } as IAds;
};

export const getReference = async (hostname = "") => {
  return getReferenceByHost(hostname);
};

export const getConfigLayout = async (theme = "") => {
  return getConfigByHost(theme);
};
export const getDetailPostWithSlug = async (pathname = "") => {
  return getDetailPostBySlug(pathname);
};
