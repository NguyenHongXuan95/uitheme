import { useEffect, useState } from "preact/hooks";
import "./style.css";

const Post = () => {
  const [theme, setTheme] = useState("default");
  useEffect(() => {
    if (typeof window.location.search.split("?theme=")[1] !== "undefined") {
      setTheme(window.location.search.split("?theme=")[1]);
    } else {
      setTheme("default");
    }
  }, [theme]);

  return (
    <div className={"buttonPanigation mobileselect"}>
      <select
        id="theme"
        onChange={(e) => {
          window.location.replace(`?theme=${e.target.value}`);
          setTheme(e.target.value);
        }}
        value={theme}
      >
        <option value="default">default</option>
        <option value="dark">dark</option>
        <option value="pastel">pastel</option>
      </select>
    </div>
  );
};
export default Post;
