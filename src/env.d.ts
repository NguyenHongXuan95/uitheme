/// <reference types="astro/client" />

declare interface IPost {
  title: string;
  excerpt: string;
  content: string;
  slug: string;
  createdAt: number;
  categories: number[];
  category: { name: string };
  author: { name: string };
}

declare interface ICategory {
  name: string;
  slug: string;
}

declare interface IAds {
  pid: string;
  unit: { [key: string]: string };
  domain: string;
}

declare interface IReference {
  url: string;
  domain: string;
}

declare interface IConfig {
  layout: string;
  domain: string;
}
declare interface ITheme {
  theme: string;
  font: string;
  color: string;
  backgroundColor: string;
  cardColor: string;
  buttonCardColor: string;
  headerColor: string;
  active: string;
}
