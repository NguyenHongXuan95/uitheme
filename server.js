import compress from "@fastify/compress";
import fastifyMiddie from "@fastify/middie";
import fastifyStatic from "@fastify/static";
import { load } from "cheerio";
import "dotenv/config";
import Fastify from "fastify";
import knex from "knex";
import fetch from "node-fetch";
import { existsSync, mkdirSync } from "node:fs";
import { fileURLToPath } from "node:url";
import { join } from "path";
import { handler as ssrHandler } from "./dist/server/entry.mjs";

const dbPath = join(process.cwd(), "wp-content");
if (!existsSync(dbPath)) {
  mkdirSync(dbPath);
}

const SCRIPT_HORIZONTAL =
  '<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-ADS_PID" crossorigin="anonymous"></script> <ins class="adsbygoogle" style="display:block" data-ad-client="ca-ADS_PID" data-ad-slot="AD_SLOT" data-ad-format="auto" data-full-width-responsive="true"></ins> <script>(adsbygoogle = window.adsbygoogle || []).push({}); </script>';

const DEFAULT_CATEGORIES = [
  { name: "Bitcoin", slug: "bitcoin" },
  { name: "Coin Market", slug: "coin_market" },
  { name: "Crypto Investment", slug: "crypto_investment" },
  { name: "CryptoCurrency", slug: "crypto_currency" },
  { name: "Dogecoin", slug: "dogecoin" },
  { name: "Ethereum", slug: "ethereum" },
  { name: "Technology", slug: "technology" },
  { name: "Trading", slug: "trading" },
];

const knexIns = knex({
  client: "better-sqlite3",
  connection: { filename: join(dbPath, "data.db") },
  useNullAsDefault: true,
});

const getCategories = () => {
  const data = [];
  while (data.length < 4) {
    const category =
      DEFAULT_CATEGORIES[Math.floor(Math.random() * DEFAULT_CATEGORIES.length)];
    if (!data.some((x) => x.slug === category.slug)) {
      data.push(category);
    }
  }
  return data;
};

const tableExist = async (tbName) => {
  return await knexIns.schema.hasTable(tbName);
};

const createCategoryTableAsync = async (tbName) => {
  return new Promise((resolve) => {
    resolve(
      knexIns.schema.createTable(tbName, function (table) {
        table.increments("id").primary();
        table.string("name", 100);
        table.string("slug", 100);
        table.datetime("createdAt");
      })
    );
  });
};

const createPostTableAsync = async (tbName) => {
  return new Promise((resolve) => {
    resolve(
      knexIns.schema.createTable(tbName, function (table) {
        table.increments("id").primary();
        table.text("content");
        table.string("title", 200);
        table.string("slug", 100);
        table.string("category", 50);
        table.string("excerpt", 200);
        table.datetime("createdAt");
      })
    );
  });
};

const createAdsTableAsync = async (tbName) => {
  return new Promise((resolve) => {
    resolve(
      knexIns.schema.createTable(tbName, function (table) {
        table.increments("id").primary();
        table.string("pid", 30);
        table.string("domain", 100);
        table.jsonb("unit");
        table.datetime("createdAt");
      })
    );
  });
};

const createReferenceTableAsync = async (tbName) => {
  return new Promise((resolve) => {
    resolve(
      knexIns.schema.createTable(tbName, function (table) {
        table.increments("id").primary();
        table.string("url", 100);
        table.string("domain", 100);
      })
    );
  });
};

const createTablesIfNotExist = async (categoryTbl, postTbl) => {
  const categoryTblExist = await tableExist(categoryTbl);
  const postTblExist = await tableExist(postTbl);
  if (!categoryTblExist) {
    await createCategoryTableAsync(categoryTbl);
    await knexIns
      .insert(getCategories().map((c) => ({ ...c, createdAt: new Date() })))
      .into(categoryTbl)
      .returning("id");
  }
  if (!postTblExist) {
    await createPostTableAsync(postTbl);
    const categories = await knexIns.select("name").from(categoryTbl);
    await seedPostsFromAPI(categories, postTbl);
  }
};

const randomSize = (min = 1, max = 9) => {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const seedPostsFromAPI = async (categories, postTbl) => {
  const posts = await Promise.all(
    categories.map(async (c) => {
      const res = await fetch(
        process.env.POST_ENDPOINT + `${c.name}&limit=1` + randomSize()
      );
      return res.json();
    })
  );

  const newPosts = shufflePosts(posts.flat());
  const today = new Date();
  for (let index = 0; index < newPosts.length; index++) {
    const p = newPosts[index];
    const createdAt = new Date();
    createdAt.setDate(today.getDate() - index);
    const content = load(p.content);

    content("h2")
      .toArray()
      .map((c, index) => {
        switch (index) {
          case 0:
            content(c).after(
              SCRIPT_HORIZONTAL.replaceAll("AD_SLOT", "horizontal1")
            );
            return;

          case 2:
            content(c).after(
              SCRIPT_HORIZONTAL.replaceAll("AD_SLOT", "horizontal2")
            );
            return;

          case 4:
            content(c).after(
              SCRIPT_HORIZONTAL.replaceAll("AD_SLOT", "horizontal3")
            );
            return;

          case 6:
            content(c).after(
              SCRIPT_HORIZONTAL.replaceAll("AD_SLOT", "horizontal4")
            );
            return;

          default:
            return;
        }
      });

    await knexIns
      .insert({
        ...p,
        excerpt: extractExcept(p.content),
        createdAt,
        content: content.html(".aw-content"),
      })
      .into(postTbl)
      .returning("id");
  }
  for (const p of newPosts) {
  }
};

const extractExcept = (content) => {
  const $ = load(content);

  const firstH2 = $("h2")[0];
  if (firstH2) {
    let nextTag = firstH2.next;
    while (true) {
      if (nextTag.name === "p") {
        break;
      }
      nextTag = nextTag.next;
    }
    return nextTag.children[0].data;
  }
  return "";
};

const shufflePosts = (posts) => {
  const newPosts = [];

  while (posts.length) {
    var randomIndex = Math.floor(Math.random() * posts.length),
      element = posts.splice(randomIndex, 1);

    newPosts.push(element[0]);
  }
  return newPosts;
};

const app = Fastify({ logger: true });

const hostnameToTbl = (hostname) => {
  return hostname.replace(/\./g, "_").split(":")[0];
};

app.get("/website-info", async (req, reply) => {
  try {
    const hostname = hostnameToTbl(req.hostname);
    const categoryTbl = hostname + "_categories";
    const postTbl = hostname + "_posts";

    return { categories: [], config: {} };
  } catch (error) {
    console.log("Website invalid!!!", error);
  }
});

app.post("/init", async (req, reply) => {
  if (process.env.POST_ENDPOINT) {
    try {
      const hostname = hostnameToTbl(req.hostname);
      const categoryTbl = hostname + "_categories";
      const postTbl = hostname + "_posts";

      await createTablesIfNotExist(categoryTbl, postTbl);
    } catch (error) {
      console.log("error", error);
      reply.send("error occurred");
    }
  }
  reply.send(hostname);
});

app.post("/init-ads", async (req, reply) => {
  if (process.env.ADS_ENDPOINT) {
    const tblExist = await tableExist("google_ads");
    if (!tblExist) {
      try {
        const arrHost = req.hostname.split(".");
        const domain = `${arrHost[arrHost.length - 2]}.${
          arrHost[arrHost.length - 1]
        }`;
        const res = await fetch(process.env.ADS_ENDPOINT + domain);
        const json = await res.json();

        const { pid, unit } = json;
        await createAdsTableAsync("google_ads");
        await knexIns
          .insert({ pid, unit, domain: req.hostname, createdAt: new Date() })
          .into("google_ads")
          .returning("id");
      } catch (error) {
        console.log("error", error);
        reply.send("error occurred");
      }
    }
  }
  reply.send(req.hostname);
});

app.post("/init-reference", async (req, reply) => {
  const referenceURL = req.body.url.toString();
  const referenceTbl = "google_reference";
  const tblExist = await tableExist(referenceTbl);
  console.log("reference true");

  if (!tblExist) {
    try {
      await createReferenceTableAsync(referenceTbl);
      await knexIns
        .insert({ url: referenceURL, domain: req.hostname })
        .into(referenceTbl)
        .returning("id");
    } catch (error) {
      console.log("error", error);
      reply.send("error occurred");
    }
  } else {
    const ref = await knexIns
      .select("*")
      .from("google_ads")
      .where({ domain: req.hostname })
      .first();
    if (!ref) {
      await knexIns
        .insert({ url: referenceURL, domain: req.hostname })
        .into(referenceTbl)
        .returning("id");
    } else {
      await knexIns
        .select("*")
        .from("google_ads")
        .where({ domain: req.hostname })
        .update({ url: referenceURL });
    }
  }
  reply.send(req.hostname);
});

await app
  .register(fastifyStatic, {
    root: fileURLToPath(new URL("./dist/client", import.meta.url)),
    cacheControl: true,
    maxAge: "7d",
  })
  .register(fastifyMiddie)
  .register(compress);

app.use(ssrHandler);

app.listen({ port: 3000, host: process.env.HOST || "127.0.0.1" });
